from django.shortcuts import render,redirect,reverse

# Create your views here.
from app01 import models
from django.http.response import JsonResponse


def lab(request):
    all_lab=models.Lab.objects.all()
    return render(request,"lab.html",{"all_lab":all_lab})


from django import forms
class RegForm(forms.Form):
    lab_name=forms.CharField(label="名称",required=True)
    floor=forms.CharField(label="楼层")
    room_number=forms.CharField(label="房间号")
    admin=forms.CharField(label="负责人")

def lab_add(request):
    form_obj=RegForm()
    if request.method=="POST":
        form_obj1=RegForm(request.POST)
        if form_obj1.is_valid():
            form_obj = form_obj1.clean()
        print(form_obj,type(form_obj))
        lab_name=form_obj.get("lab_name")
        if models.Lab.objects.filter(lab_name=lab_name).first():
           error="名字已经存在"
           return render(request, "lab_add.html", {"form_obj": form_obj1,"error":error})
        else:
            models.Lab.objects.create(lab_name=form_obj.get("lab_name"),floor=form_obj.get("floor"), room_number=form_obj.get("room_number"),admin=form_obj.get("admin"))
            return redirect(reverse(lab))
    return render(request,"lab_add.html",{"form_obj":form_obj})


def lab_edit(request,pk):
    error = ""
    lab_obj=models.Lab.objects.filter(pk=pk).first()
    print(lab_obj)
    if request.method=="POST":
        lab_name=request.POST.get("lab_name")
        floor=request.POST.get("floor")
        room_number=request.POST.get("room_number")
        admin=request.POST.get("admin")
        if not lab_obj.lab_name:
            error="名字不能为空"
        elif  lab_obj.lab_name ==lab_name or models.Lab.objects.filter(lab_name=lab_name):
            error="名字已经存在"
        else:
            lab_obj.lab_name = lab_name
            lab_obj.floor =floor
            lab_obj.room_number = room_number
            lab_obj.admin =admin
            lab_obj.save()
            return redirect(reverse(lab))
    return render(request,"lab_edit.html",{"lab_obj":lab_obj,"error":error})



def del1(request,name,pk):
    print(111)
    model_class = getattr(models,name)
    model_class.objects.filter(pk=pk).delete()
    return JsonResponse({'status':200,'msg':'ok'})


def assert1(request):
    all_assert=models.Assert.objects.all()
    return render(request, "assert1.html", {"all_assert":all_assert})






def user(request):
    all_user=models.User.objects.all()
    return render(request,"user.html",{"all_user":all_user})





