from django.db import models

# Create your models here.
#设计表结构：
# 实验室表 物资表 用户表
# 实验室表包含内容：名称、楼层、房间号、负责人
# 资产表包含内容：物资名称、物资描述、物资编号、购买时间、购买人
# 用户表包含内容：用户名、密码、姓名、手机号
# 实验室表
class Lab(models.Model):
    lab_name = models.CharField(max_length=32,verbose_name="名称")
    floor = models.CharField(max_length=32,verbose_name="楼层")
    room_number = models.CharField(max_length=32,verbose_name="房间号")
    admin=models.CharField(max_length=32,verbose_name="负责人")

# 资产表        一个资产只能属于一个实验室，一个实验室可以包括多个资产
class Assert(models.Model):
    ass_name = models.CharField(max_length=32,verbose_name="物资名称")
    description = models.TextField(max_length=255,verbose_name="物资描述")
    ass_id = models.CharField(max_length=32,unique=True,verbose_name="物资编号")
    buy_time = models.DateTimeField(verbose_name="购买时间")
    buyer = models.CharField(max_length=32,verbose_name="购买人")
    lab = models.ForeignKey("Lab",on_delete=models.CASCADE)

# 用户表   一个实验室可以包括多个学生用户，一个用户可以在多个实验室上课
class User(models.Model):
    user_name=models.CharField(max_length=32,verbose_name="用户名")
    pwd = models.CharField(max_length=6,verbose_name="密码")
    true_name = models.CharField(max_length=32,verbose_name="姓名")
    phone_num = models.CharField(max_length=11,verbose_name="手机号")
    lab=models.ManyToManyField("Lab")





